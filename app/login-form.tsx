"use client";

import {
  Button,
  Card,
  Container,
  Input,
  Link,
  Row,
  Spacer,
  StyledUserName,
  Text, useInput
} from "@nextui-org/react";
import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";

export default function LoginForm() {
  const router = useRouter();
  const email = useInput("");
  const password = useInput("");
  const onSubmit = async () => {
    const result = await signIn("credentials", {
      username: email.value,
      password: password.value,
      redirect: true,
      callbackUrl: "/browse_games",
    });

  };
  const validateFields = () => {
    return password.value.length > 0
      && email.value.length > 0;
  }

  return (
    <Container
      display="flex"
      alignItems="center"
      justify="center"
      css={{ minHeight: "100vh" }}
    >
      <Card css={{ mw: "420px", p: "20px", borderRadius: 0 }}>
        <Text
          size={24}
          weight="bold"
          css={{
            as: "center",
            mb: "20px",
          }}
        >
          Log in to your account
        </Text>
        <Input
          {...email.bindings}
          clearable
          underlined
          fullWidth
          color="primary"
          size="lg"
          placeholder="Username or email"
        />
        <Spacer y={1} />
        <Input
          {...password.bindings}
          clearable
          underlined
          fullWidth
          color="primary"
          size="lg"
          placeholder="Required"
          type="password"
        />
        <Spacer y={1} />
        <Row>
          <Button disabled={!validateFields()} onClick={onSubmit} css={{ borderRadius: 0 }} auto>Log in</Button>
          <Spacer x={1} /> or <Spacer x={1} /><Link href="/signup">Create Account</Link>
        </Row>
      </Card>
    </Container>
  );
}

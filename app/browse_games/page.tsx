import BrowseGames from "../components/browse_games";

export default async function BrowseGamesPage({
    searchParams,
}: {
    searchParams?: { [key: string]: string | undefined };
}) {
    return <BrowseGames searchParams={searchParams}></BrowseGames>;
}


import Game from "../../components/game";



export default async function GamePage({
    params,
}: {
    params: { name: string };
}) {
    return <Game pathParamName={params.name}></Game>;
}

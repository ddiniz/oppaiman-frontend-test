import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";
import { authOptions } from "./api/auth/[...nextauth]/route";
import LoginForm from "./login-form";

export default async function Home() {
  const session = await getServerSession(authOptions);
  if (session) {
    redirect("home/client");
  }

  return <LoginForm></LoginForm>;
}

import { BASE_PATH } from "./api";
import { WithSWR, fetcher } from "./baseapi";
import {
    FilterGameRequest,
    GameRequest,
    GameResponse,
    PaginationGames,
} from "./model/game";
import { objectToQueryParams } from "./utils";
export const PATH_LIST_GAMES_GET = () => {
    return `${BASE_PATH}/games`;
};
export const PATH_LIST_GAMES_PAGINATED_GET = () => {
    return `${BASE_PATH}/games/paginated`;
};
export const PATH_FIND_GAME_BY_ID_GET = (id: string) => {
    return `${BASE_PATH}/games/${id}`;
};
export const PATH_FIND_GAME_BY_PAGEURL_GET = (pageUrl: string) => {
    return `${BASE_PATH}/games/page/${pageUrl}`;
};
export const PATH_CREATE_GAME_DEV_POST = () => {
    return `${BASE_PATH}/games_dev`;
};
export const PATH_UPDATE_GAME_DEV_PUT = (id: string) => {
    return `${BASE_PATH}/games_dev/${id}`;
};
export const PATH_DELETE_GAME_DEV_DELETE = (id: string) => {
    return `${BASE_PATH}/games_dev/${id}`;
};
export const PATH_GET_USER_GAMES = () => {
    return `${BASE_PATH}/games/user`;
};



export function getListGames(): {
    data: GameResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<GameResponse>({ url: PATH_LIST_GAMES_GET(), method: "GET" });
}

export function getListGamesPaginated(request: FilterGameRequest): {
    data: PaginationGames;
    error: any;
    isLoading: any;
} {
    let queryParams = objectToQueryParams(request);
    return WithSWR<PaginationGames>({
        url: PATH_LIST_GAMES_PAGINATED_GET() + queryParams,
        method: "GET",
    });
}
export function getFindGameById(id: string): {
    data: GameResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<GameResponse>({
        url: PATH_FIND_GAME_BY_ID_GET(id),
        method: "GET",
    });
}
export function getFindGameByPageUrl(pageUrl: string): {
    data: GameResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<GameResponse>({
        url: PATH_FIND_GAME_BY_PAGEURL_GET(pageUrl),
        method: "GET",
    });
}
export function postCreateGameDev(
    createGameRequest: FormData,
    token: string
): Promise<Response> {
    return fetcher({
        url: PATH_CREATE_GAME_DEV_POST(),
        method: "POST",
        authToken: token,
        requestBody: createGameRequest,
    });
}
export function putUpdateGameDev(
    id: string,
    updateGameRequest: GameRequest,
    token: string
): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_UPDATE_GAME_DEV_PUT(id),
        method: "PUT",
        authToken: token,
        requestBody: updateGameRequest,
    });
}
export function deleteGameDev(
    id: string,
    token: string
): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_DELETE_GAME_DEV_DELETE(id),
        method: "DELETE",
        authToken: token,
    });
}

export function getUserGames(token: string): {
    data: GameResponse[];
    error: any;
    isLoading: any;
} {
    return WithSWR<GameResponse[]>({
        url: PATH_GET_USER_GAMES(),
        method: "GET",
        authToken: token,
    });
}

import useSWR from "swr";

type Method = "GET" | "POST" | "PUT" | "DELETE";
export interface SWRRequest {
  url: string;
  method: Method;
  requestBody?: any;
  authToken?: string;
}

const appendBody = (requestBody: any) => {
  if (!requestBody) return undefined;

  if (requestBody instanceof FormData) return requestBody;

  return JSON.stringify(requestBody);
};

export const fetcher = async (request: SWRRequest) => {
  const header = request.authToken
    ? {
      Authorization: `Bearer ${request.authToken}`,
    }
    : undefined;

  const res = await fetch(request.url, {
    method: request.method,
    body: appendBody(request.requestBody),
    headers: header,
  });
  if (!res.ok) {
    const bodyText = await res.text();
    const error = new Error(`Error: ${res.status} - ${bodyText}`);
    throw error;
  }
  const bodyText = await res.text();
  try {
    return JSON.parse(bodyText);
  } catch (e) {
    return bodyText;
  }
};

export function WithSWR<T>(request: SWRRequest): {
  data: T;
  error: any;
  isLoading: any;
} {
  return useSWR(request, fetcher);
}

export function WithSWR2<T>(
  doRequest: boolean,
  request: SWRRequest
): {
  data: T;
  error: any;
  isLoading: any;
} {
  return useSWR(doRequest ? request : null, fetcher);
}

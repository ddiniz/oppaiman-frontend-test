export interface OSRequest {
    name: string;
    description: string;
}

export interface OSResponse {
    id: number;
    name: string;
    description: string;
}
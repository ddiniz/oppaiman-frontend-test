export interface BuildRequest {
    name: string;
    description: string;
    version: string;
    file: string;
}
export interface BuildResponse {
    id: number;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
    name: string;
    description: string;
    version: string;
    file: string;
}
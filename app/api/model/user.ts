export interface LoginRequest {
    usernameOrEmail: string;
    password: string;
}

export interface UserRequest {
    username: string;
    email: string;
    password: string;
    role: number;
}

export interface UserResponse {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    email: string;
    username: string;
    role: number;
}

export interface Pagination {
    limit: number;
    page: number;
    sort: string;
    total_rows: number;
    total_pages: number;
    rows: any;
}
export interface ImageRequest {
    name: string;
    src: string;
}
export interface ImageResponse {
    id: number;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
    name: string;
    src: string;
}

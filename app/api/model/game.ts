import { BuildRequest, BuildResponse } from "./build";
import { GenreRequest, GenreResponse } from "./genre";
import { ImageRequest, ImageResponse } from "./image";
import { OSRequest, OSResponse } from "./os";
import { PriceRequest, PriceResponse } from "./price";

export interface PaginationGames {
    limit: number;
    page: number;
    sort: string;
    total_rows: number;
    total_pages: number;
    rows: GameResponse[];
}

export interface GameResponse {
    id: string;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
    description: string;
    shortDescription: string;
    title: string;
    author: string;
    pageUrl: string;
    status: string;
    prices: PriceResponse[];
    builds: BuildResponse[];
    images: ImageResponse[];
    genres: GenreResponse[];
    oses: OSResponse[];
    catalogImage: ImageResponse;
    headerImage: ImageResponse;
}

export interface GameRequest {
    description: string;
    shortDescription: string;
    title: string;
    pageUrl: string;
    status: string;
    prices: PriceRequest[];
    builds: BuildRequest[];
    images: ImageRequest[];
    genres: GenreRequest[];
    oses: OSRequest[];
    catalogImage: ImageRequest;
    headerImage: ImageRequest;
}

export interface FilterGameRequest {
    name?: string;
    price?: string;
    onSale?: string;
    releaseDate?: string;
    genre?: string;
    limit?: string;
    page?: string;
}

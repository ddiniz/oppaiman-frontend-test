export interface PriceRequest {
    region: string;
    value: number;
}

export interface PriceResponse {
    id: number;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
    region: string;
    value: number;
}
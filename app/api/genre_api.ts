import { BASE_PATH } from "./api";
import { WithSWR } from "./baseapi";
import { GenreResponse, GenreRequest } from "./model/genre";


export const PATH_GENRE_LIST_GET = () => { return `${BASE_PATH}/genres`; }
export const PATH_GENRE_GET = (id: number) => { return `${BASE_PATH}/genres/${id}`; }
export const PATH_GENRE_DEV_POST = () => { return `${BASE_PATH}/genres_admin`; }
export const PATH_GENRE_DEV_PUT = (id: number) => { return `${BASE_PATH}/genres_admin/${id}`; }
export const PATH_GENRE_DEV_DELETE = (id: number) => { return `${BASE_PATH}/genres_admin/${id}`; }


export function listGenres(): {
    data: GenreResponse[];
    error: any;
    isLoading: any;
} {
    return WithSWR<GenreResponse[]>({ url: PATH_GENRE_LIST_GET(), method: "GET" });
}
export function getGenres(id: number): {
    data: GenreResponse[];
    error: any;
    isLoading: any;
} {
    return WithSWR<GenreResponse[]>({ url: PATH_GENRE_GET(id), method: "GET" });
}
export function postGenreDev(
    createGenreRequest: GenreRequest,
    token: string
): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_GENRE_DEV_POST(),
        method: "POST",
        authToken: token,
        requestBody: createGenreRequest,
    });
}
export function putGenreDev(id: number,
    updateGenreRequest: GenreRequest,
    token: string
): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_GENRE_DEV_PUT(id),
        method: "PUT",
        authToken: token,
        requestBody: updateGenreRequest,
    });
}
export function deleteGenreDev(id: number, token: string): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_GENRE_DEV_DELETE(id),
        method: "DELETE",
        authToken: token,
    });
}
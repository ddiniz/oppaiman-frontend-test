import { ImageResponse } from "next/server";
import { BASE_PATH } from "./api";
import { WithSWR } from "./baseapi";
import { ImageRequest } from "./model/image";

export const PATH_IMAGE_GET = (id: number) => { return `${BASE_PATH}/images/${id}`; }
export const PATH_IMAGE_DEV_POST = () => { return `${BASE_PATH}/images_dev`; }
export const PATH_IMAGE_DEV_PUT = (id: number) => { return `${BASE_PATH}/images_dev/${id}`; }
export const PATH_IMAGE_DEV_DELETE = (id: number) => { return `${BASE_PATH}/images_dev/${id}`; }
export function getImage(id: number): {
    data: ImageResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<ImageResponse>({ url: PATH_IMAGE_GET(id), method: "GET" });
}
export function postImageDev(
    createImageRequest: ImageRequest,
    token: string
): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_IMAGE_DEV_POST(),
        method: "POST",
        authToken: token,
        requestBody: createImageRequest,
    });
}
export function putImageDev(
    id: number,
    updateImageRequest: ImageRequest,
    token: string
): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_IMAGE_DEV_PUT(id),
        method: "PUT",
        authToken: token,
        requestBody: updateImageRequest,
    });
}
export function deleteImageDev(id: number, token: string): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_IMAGE_DEV_DELETE(id),
        method: "DELETE",
        authToken: token,
    });
}

import useSWR from "swr";
import { BASE_PATH } from "./api";
import { fetcher, SWRRequest, WithSWR, WithSWR2 } from "./baseapi";
import { Pagination, UserRequest, UserResponse } from "./model/user";

export const PATH_CREATE_USER_POST = () => { return `${BASE_PATH}/users`; }
export const PATH_FIND_SELF_GET = () => { return `${BASE_PATH}/users_self`; }
export const PATH_UPDATE_SELF_PUT = () => { return `${BASE_PATH}/users_self`; }
export const PATH_DELETE_SELF_DELETE = () => { return `${BASE_PATH}/users_self`; }
export const PATH_LIST_USERS_ADMIN_GET = () => { return `${BASE_PATH}/users_admin`; }
export const PATH_LIST_USERS_PAGINATED_ADMIN_GET = () => { return `${BASE_PATH}/users_admin/paginated`; }
export const PATH_FIND_USER_BY_ID_ADMIN_GET = (id: string) => { return `${BASE_PATH}/users_admin/${id}`; }
export const PATH_FIND_USER_BY_USERNAME_ADMIN_GET = (username: string) => { return `${BASE_PATH}/users_admin/username/${username}`; }
export const PATH_FIND_USER_BY_EMAIL_ADMIN_GET = (email: string) => { return `${BASE_PATH}/users_admin/email/${email}`; }
export const PATH_UPDATE_USER_ADMIN_PUT = (id: string) => { return `${BASE_PATH}/users_admin/${id}`; }
export const PATH_DELETE_USER_ADMIN_DELETE = (id: string) => { return `${BASE_PATH}/users_admin/${id}`; }
export const PATH_BUY_GAME = (gameId: string) => { return `${BASE_PATH}/users_self/buy_game/${gameId}`; }
export const PATH_GET_USER_HAS_GAME = (id: string) => { return `${BASE_PATH}/users_self/game/${id}`; };
export const PATH_GET_GAME = (gameId: string) => { return `${BASE_PATH}/users_self/getgame/${gameId}`; };

export function postCreateUser(
    createUserRequest: UserRequest
): Promise<Response> {
    return fetcher({
        url: PATH_CREATE_USER_POST(),
        method: "POST",
        requestBody: createUserRequest,
    });
}
export function getFindSelf(token: string): {
    data: UserResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<UserResponse>({
        url: PATH_FIND_SELF_GET(),
        method: "GET",
        authToken: token,
    });
}
export function putUpdateSelf(
    updateUserRequest: UserRequest,
    token: string
): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_UPDATE_SELF_PUT(),
        method: "PUT",
        authToken: token,
        requestBody: updateUserRequest,
    });
}
export function deleteSelf(token: string): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_DELETE_SELF_DELETE(),
        method: "DELETE",
        authToken: token,
    });
}
export function getListUsersAdmin(token: string): {
    data: UserResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<UserResponse>({
        url: PATH_LIST_USERS_ADMIN_GET(),
        method: "GET",
        authToken: token,
    });
}
export function getListUsersPaginatedAdmin(token: string): {
    data: Pagination;
    error: any;
    isLoading: any;
} {
    return WithSWR<Pagination>({
        url: PATH_LIST_USERS_PAGINATED_ADMIN_GET(),
        method: "GET",
        authToken: token,
    });
}
export function getFindUserByIdAdmin(id: string, token: string): {
    data: UserResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<UserResponse>({
        url: PATH_FIND_USER_BY_ID_ADMIN_GET(id),
        method: "GET",
        authToken: token,
    });
}
export function getFindUserByUsernameAdmin(username: string, token: string): {
    data: UserResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<UserResponse>({
        url: PATH_FIND_USER_BY_USERNAME_ADMIN_GET(username),
        method: "GET",
        authToken: token,
    });
}
export function getFindUserByEmailAdmin(email: string, token: string): {
    data: UserResponse;
    error: any;
    isLoading: any;
} {
    return WithSWR<UserResponse>({
        url: PATH_FIND_USER_BY_EMAIL_ADMIN_GET(email),
        method: "GET",
        authToken: token,
    });
}
export function putUpdateUserAdmin(id: string, token: string): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_UPDATE_USER_ADMIN_PUT(id),
        method: "PUT",
        authToken: token,
    });
}
export function deleteUserAdmin(id: string, token: string): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR<string>({
        url: PATH_DELETE_USER_ADMIN_DELETE(id),
        method: "DELETE",
        authToken: token,
    });
}
export function buyGame(doRequest: boolean, gameId: string, token: string): {
    data: string;
    error: any;
    isLoading: any;
} {
    return WithSWR2<string>(doRequest, {
        url: PATH_BUY_GAME(gameId),
        method: "PUT",
        authToken: token,
    });
}
export function getUserHasGame(gameId: string, token: string): {
    data: boolean;
    error: any;
    isLoading: any;
} {
    return WithSWR<boolean>({
        url: PATH_GET_USER_HAS_GAME(gameId),
        method: "GET",
        authToken: token,
    });
}
export function getGame(doRequest: boolean, gameId: string, token: string, fetcher: (request: SWRRequest) => Promise<any>): {
    data: boolean;
    error: any;
    isLoading: any;
} {
    return useSWR(doRequest ? {
        url: PATH_GET_GAME(gameId),
        method: "GET",
        authToken: token,
    } : null, fetcher);
}
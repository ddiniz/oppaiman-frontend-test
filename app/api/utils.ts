import { FilterGameRequest } from "./model/game";

export function objectToQueryParams(object: any): string {
    let params = "";

    Object.entries(object).forEach(([key, value]) => {
        if (value) {
            params += params ? "&" : "?";
            params += `${key}=${value}`;
        }
    });

    return params;
}
export function getFilterGameRequestFromPath(str: string): FilterGameRequest {
    let firstSplit = str.split("?");
    let filter: FilterGameRequest = {
        name: "",
        price: "-1",
        onSale: "false",
        releaseDate: "0",
        genre: "",
        limit: "10",
        page: "1",
    };
    if (
        firstSplit != null &&
        firstSplit != undefined &&
        firstSplit.length === 2
    ) {
        let queries = firstSplit[1].split("&");
        let queryMap = new Map<string, string>();
        for (const q of queries) {
            let keyValue = q.split("=");
            if (keyValue.length != 2) continue;
            queryMap.set(keyValue[0], keyValue[1]);
        }
        let keys = Object.keys(filter) as (keyof typeof filter)[];
        for (const k of keys) {
            if (queryMap.has(k)) (filter[k] as any) = queryMap.get(k);
        }
    }
    return filter;
}

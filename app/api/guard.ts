
export const guard = (session: any, pathname: any, router: any) => {

    if (session?.data?.user?.role === 0) {
        const signUpPath = pathname == "/signup";
        const loginPath = pathname == "/login";
        const browseGamePath = pathname == "/browse_game";
        const libraryPath = pathname == "/games_library";
        if (!signUpPath && !loginPath && !browseGamePath && !libraryPath) {
            goToBrowseGames(router);
        }
    }
}

const goToBrowseGames = (router: any) => {
    router.push("/browse_games")
}
import { } from "next/server";
import { WithSWR } from "./baseapi";
import {
  LoginRequest,
} from "./model/user";

export const BASE_PATH = "http://localhost:8080";
export const PATH_HEALTH_GET = () => { return `${BASE_PATH}/health`; }
export const PATH_LOGIN_POST = () => { return `${BASE_PATH}/login`; }

export function getHealth(): {
  data: string;
  error: any;
  isLoading: any;
} {
  return WithSWR<string>({ url: PATH_HEALTH_GET(), method: "GET" });
}
export function postLogin(loginRequest: LoginRequest): {
  data: string;
  error: any;
  isLoading: any;
} {
  return WithSWR<string>({ url: PATH_LOGIN_POST(), method: "POST" });
}






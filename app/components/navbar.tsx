"use client";
import { Navbar, Text, Dropdown, Avatar, Button, Input, Spacer } from "@nextui-org/react";
import { signOut, useSession } from "next-auth/react";
import { usePathname, useRouter } from "next/navigation";
import { guard } from "../api/guard";
import { useState } from "react";
import { objectToQueryParams, getFilterGameRequestFromPath } from "../api/utils";


export const AcmeLogo = () => (
    <svg
        className=""
        fill="none"
        height="36"
        viewBox="0 0 32 32"
        width="36"
        xmlns="http://www.w3.org/2000/svg"
    >
        <rect fill="var(--secondary)" height="100%" rx="16" width="100%" />
        <path
            clipRule="evenodd"
            d="M17.6482 10.1305L15.8785 7.02583L7.02979 22.5499H10.5278L17.6482 10.1305ZM19.8798 14.0457L18.11 17.1983L19.394 19.4511H16.8453L15.1056 22.5499H24.7272L19.8798 14.0457Z"
            fill="currentColor"
            fillRule="evenodd"
        />
    </svg>
);
export const SearchIcon = ({ size, fill, width = 24, height = 24, ...props }) => {
    return (
        <svg fill="none" height={size || height} viewBox="0 0 24 24" width={size || width} {...props}>
            <path
                d="M11.5 21a9.5 9.5 0 1 0 0-19 9.5 9.5 0 0 0 0 19ZM22 22l-2-2"
                stroke={fill}
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
            />
        </svg>
    );
};
export default function MyNavbar() {
    const session = useSession();
    const pathname = usePathname();
    const router = useRouter();
    const [searchName, setSearchName] = useState("");
    guard(session, pathname, router);

    const onLogout = async () => {
        await signOut();
    };

    const goTo = (path: string) => {
        router.push(path)
    };
    const goToLogin = () => {
        goTo("/");
    };
    const goToSignup = () => {
        goTo("/signup");
    };
    const goToLibrary = () => {
        goTo("/games_library");
    };
    const goToSettings = () => {
        goTo("/settings");
    };
    const goToNewGame = () => {
        goTo("/create_game");
    };
    const goToBrowseGames = (params: string) => {
        goTo("/browse_games" + params);
    };

    const devOptions = (role: any) => {
        if (role === 0) {
            return ""
        } else {
            return <Dropdown.Item key="new_game" withDivider>
                <Button css={{ borderRadius: 0 }} light auto onClick={goToNewGame}>Create new Game</Button>
            </Dropdown.Item>
        }
    };
    const userMenu = () => {
        return <Navbar.Content>
            <Dropdown placement="bottom-right">
                <Navbar.Item>
                    <Dropdown.Trigger>
                        <Avatar
                            bordered
                            as="button"
                            color="primary"
                            size="md"
                            src="https://i.pravatar.cc/150?u=a042581f4e29026704d"
                        />
                    </Dropdown.Trigger>
                </Navbar.Item>
                <Dropdown.Menu
                    aria-label="User menu actions"
                    color="secondary"
                >
                    <Dropdown.Item key="profile" css={{ height: "$18" }}>
                        <Text b color="inherit" css={{ d: "flex" }}>
                            Signed in as
                        </Text>
                        <Text b color="inherit" css={{ d: "flex" }}>
                            {session?.data?.user?.name}
                        </Text>
                    </Dropdown.Item>
                    <Dropdown.Item key="library" withDivider>
                        <Button css={{ borderRadius: 0 }} light auto onClick={goToLibrary}>My Library</Button>
                    </Dropdown.Item>
                    <Dropdown.Item key="settings" withDivider>
                        <Button css={{ borderRadius: 0 }} light auto onClick={goToSettings}>My Settings</Button>
                    </Dropdown.Item>
                    {devOptions(session?.data?.user?.role)}
                    <Dropdown.Item key="logout" color="error" withDivider >
                        <Button css={{ borderRadius: 0 }} light auto onClick={onLogout}>Log Out</Button>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </Navbar.Content>
    };
    const signInSignUp = () => {
        return <Navbar.Content>
            <Button css={{ borderRadius: 0 }} ghost auto onClick={goToLogin}>Login</Button>
            <Button css={{ borderRadius: 0 }} ghost auto onClick={goToSignup}>Signup</Button>
        </Navbar.Content>
    };
    const loginContent = () => {
        if (session.status === "authenticated") {
            return userMenu()
        } else {
            if (pathname !== "/") {
                return signInSignUp()
            } else {
                return <div></div>
            }
        }
    };
    return (
        <Navbar isBordered maxWidth="xl" variant="static">
            <Navbar.Brand>
                <AcmeLogo />
                <Text b color="inherit">
                    PLACEHOLDER
                </Text>
                <Navbar.Content activeColor="primary" variant="underline">
                    <Spacer x={1}></Spacer>
                    <Navbar.Link css={{ p: 20 }} {...{ isActive: pathname === "/browse_games" }}
                        href="/browse_games">Browse games</Navbar.Link>
                    <Navbar.Item>
                        <Input
                            clearable
                            contentLeft={
                                <SearchIcon fill="var(--nextui-colors-accents6)" size={16} />
                            }
                            contentLeftStyling={false}
                            css={{
                                w: "100%",
                                "@xsMax": {
                                    mw: "300px",
                                },
                                "& .nextui-input-content--left": {
                                    h: "100%",
                                    ml: "$4",
                                    dflex: "center",
                                },
                            }}
                            onChange={(e) => (setSearchName(e.target.value))}
                            placeholder="Search for games"
                            onKeyDown={(e) => {
                                if (e.key === "Enter") {
                                    let filter = getFilterGameRequestFromPath(window.location.toString());
                                    filter.name = searchName;
                                    goToBrowseGames(objectToQueryParams(filter));
                                }
                            }}
                        />
                    </Navbar.Item>
                </Navbar.Content>
            </Navbar.Brand>

            {loginContent()}

        </Navbar >
    );
}

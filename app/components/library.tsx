"use client";
import { useRouter } from "next/navigation";
import { Card, Col, Container, Row, Text, Grid, Spacer } from "@nextui-org/react";
import { getUserGames } from "../api/game_api";
import jwt from 'jwt-decode'
import { useSession } from "next-auth/react";
import { BASE_PATH } from "../api/api";

export default function Library() {
    const session = useSession();
    const router = useRouter();
    const token = session.data?.jwt;
    const { data, error, isLoading } = getUserGames(token);
    if (error) return "An error has occurred.";
    const renderUserGames = () => {
        if (isLoading) return <div>Loading...</div>;
        return data.map((item, i) => {
            return <Grid css={{ width: "300px" }} key={item.id}>
                <Card
                    isPressable
                    onPress={() => {
                        router.push(`/game/${item.pageUrl}`);
                    }}
                >
                    <Card.Body css={{ p: 0 }}>
                        <Card.Image
                            src={`${BASE_PATH}/Content/${item.id}/${item.catalogImage.id}`}
                            objectFit="cover"
                            width="100%"
                            height={250}
                            alt={item.title}
                        />
                    </Card.Body>
                    <Card.Footer css={{ justifyItems: "flex-start" }}>
                        <Col>
                            <Row justify="space-between" align="center">
                                <Text b>{item.title}</Text>
                            </Row>
                            <Row justify="space-between" align="center">
                                <Text >{item.author}</Text>
                            </Row>
                        </Col>
                    </Card.Footer>
                </Card>
            </Grid>
        })
    }
    return <Container css={{ minHeight: "100vh", width: "60vw", backgroundColor: "#FFFFFF" }}>
        <Row>
            <Col>
                <Spacer y={1} />
                <Text size={24} weight="bold">My Library</Text>
                <Grid.Container gap={2} justify="flex-start">
                    {renderUserGames()}
                </Grid.Container>
            </Col>
        </Row>

    </Container>;
}

"use client";
import {
    Button,
    Card,
    Col,
    Container,
    Dropdown,
    Input,
    Row,
    Spacer,
    Switch,
    Text,
    Textarea,
    Image,
    useInput
} from "@nextui-org/react";
import React, { useRef } from "react";
import CurrencyInput from "react-currency-input-field";
import { listGenres } from "../api/genre_api";
import { listOSes } from "../api/os_api";

import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { FileUploader } from "react-drag-drop-files";
import { postCreateGameDev } from "../api/game_api";
import { GenreRequest, GenreResponse } from "../api/model/genre";
import { OSRequest, OSResponse } from "../api/model/os";

const handleFiles = (files: any) => {
    return [...files].map((file) => {
        return {
            src: URL.createObjectURL(file),
            file: file,
        };
    });
};

export default function CreateGame() {
    const session = useSession();
    const router = useRouter();
    const fileTypes = ["JPG", "PNG", "GIF"];
    const fileTypesGame = ["RAR", "ZIP", "EXE"];

    const handleChangeCover = (files: File) => {
        setCoverFile(handleFiles([files]));
    };
    const handleChangeHeader = (files: File) => {
        setHeaderFile(handleFiles([files]));
    };
    const handleChangeScreenshots = (files: FileList) => {
        setScreenshots(handleFiles(files));
    };
    const handleGameChange = (files: File) => {
        setGameFile(handleFiles([files]));
    };

    const [headerFile, setHeaderFile] = React.useState([]);
    const [coverFile, setCoverFile] = React.useState([]);
    const [screenshots, setScreenshots] = React.useState([]);
    const [gameFile, setGameFile] = React.useState([]);
    const title = useInput("");
    const pageUrl = useInput("");
    const shortDescription = useInput("");
    const description = useInput("");
    const price = useInput("15.00");

    const goTo = (path: string) => {
        router.push(path);
    };
    const onSubmit = () => {
        let genres: GenreRequest[] = [];
        selectedGenres.forEach((genre) => {
            genres.push({
                name: genre,
                description: "",
            });
        });

        let oses: OSRequest[] = [];
        selectedOSes.forEach((os) => {
            oses.push({
                name: os,
                description: "",
            });
        });

        let statusIterator = selectedStatus.values();

        let gamePrice = "0";
        if (!freeGame) {
            gamePrice = price.value;
        }

        const gamedata = {
            title: title.value,
            pageUrl: pageUrl.value.replaceAll(" ", "").replaceAll("\t", ""),
            shortDescription: shortDescription.value,
            description: description.value,
            prices: [{ value: parseFloat(gamePrice), region: "Brazil" }],
            status: statusIterator.next().value,
            builds: [{
                name: "Ver1.0",
                description: "First build",
                version: "Ver1.0",
            }],
            genres: genres,
            oses: oses,
        };

        const formData = new FormData();

        formData.append("gamedata", JSON.stringify(gamedata));
        screenshots.forEach((ss, i) => {
            formData.append("screenshot" + i, ss.file);
        });
        coverFile.forEach((cf, i) => {
            formData.append("catalogImage", cf.file);
        });
        headerFile.forEach((hf, i) => {
            formData.append("headerImage", hf.file);
        });
        gameFile.forEach((gf, i) => {
            formData.append("game", gf.file);
        });

        postCreateGameDev(formData, session.data?.jwt).then(() => {
            goTo(`/game/${gamedata.pageUrl}`);
        });
    };
    const [freeGame, setFreeGame] = React.useState(false);
    const [selectedStatus, setSelectedStatus] = React.useState(
        new Set(["Released"])
    );
    const [selectedGenres, setSelectedGenres] = React.useState(new Set(["RPG"]));
    const [selectedOSes, setSelectedOS] = React.useState(new Set(["Win11"]));

    const selectedStatusValue = React.useMemo(
        () => Array.from(selectedStatus).join(", ").replaceAll("_", " "),
        [selectedStatus]
    );
    const selectedGenreValue = React.useMemo(
        () => Array.from(selectedGenres).join(", ").replaceAll("_", " "),
        [selectedGenres]
    );
    const genreData = listGenres();
    const renderGenre = () => {
        if (genreData.isLoading) return <div>Loading...</div>;
        return genreData.data.map((item: GenreResponse) => (
            <Dropdown.Item key={item.name}>{item.name}</Dropdown.Item>
        ));
    };

    const selectedOSValue = React.useMemo(
        () => Array.from(selectedOSes).join(", ").replaceAll("_", " "),
        [selectedOSes]
    );
    const osData = listOSes();
    const renderOSes = () => {
        if (osData.isLoading) return <div>Loading...</div>;
        return osData.data.map((item: OSResponse) => (
            <Dropdown.Item key={item.name}>{item.name}</Dropdown.Item>
        ));
    };
    const validateFields = () => {
        return title.value.length > 0
            && pageUrl.value.length > 0
            && shortDescription.value.length > 0
            && description.value.length > 0;
    }
    return (
        <Container
            display="flex"
            alignItems="center"
            justify="center"
            css={{ minHeight: "100vh" }}
        >
            <Card css={{ mw: "70vw", p: "20px", borderRadius: 0 }} variant="bordered">
                <Card.Header>
                    <Text
                        size={24}
                        weight="bold"
                        css={{
                            as: "center",
                            mb: "20px",
                        }}
                    >
                        Create a new game
                    </Text>
                </Card.Header>
                <Row>
                    <Col span={8} css={{ m: 10 }}>
                        <Input
                            {...title.bindings}
                            labelPlaceholder="Title"
                            clearable
                            underlined
                            fullWidth
                            color="primary"
                            size="lg"
                            placeholder="Title"
                        />
                        <Spacer y={2} />
                        <Input
                            {...pageUrl.bindings}
                            labelPlaceholder="Game page url"
                            clearable
                            underlined
                            fullWidth
                            color="primary"
                            size="lg"
                            placeholder="mygamename"
                        />
                        <Spacer y={2} />
                        <Input
                            {...shortDescription.bindings}
                            labelPlaceholder="Short description or tagline"
                            clearable
                            underlined
                            fullWidth
                            color="primary"
                            size="lg"
                            placeholder="Optional"
                        />
                        <Spacer y={2} />
                        <Dropdown>
                            <Dropdown.Button
                                flat
                                color="secondary"
                                css={{ tt: "capitalize" }}
                            >
                                {selectedStatusValue}
                            </Dropdown.Button>
                            <Dropdown.Menu
                                aria-label="Release status"
                                color="secondary"
                                disallowEmptySelection
                                selectionMode="single"
                                selectedKeys={selectedStatus}
                                onSelectionChange={setSelectedStatus}
                            >
                                <Dropdown.Item key="Released">Released</Dropdown.Item>
                                <Dropdown.Item key="Alpha">Alpha</Dropdown.Item>
                                <Dropdown.Item key="Beta">Beta</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                        <Spacer y={2} />
                        <Text size={24}>Pricing</Text>
                        <Row>
                            <Switch
                                squared
                                color="primary"
                                checked={freeGame}
                                onChange={(e) => {
                                    setFreeGame(e.target.checked);
                                }}
                            />
                            <Spacer x={0.5} />
                            <Text b css={{ mt: 3 }}>
                                {" "}
                                Free
                            </Text>
                        </Row>
                        <Row>
                            <CurrencyInput
                                {...price.bindings}
                                hidden={freeGame}
                                style={{
                                    outline: "none",
                                    padding: "6px",
                                    width: "100%",
                                    borderBottom: "2px solid rgba(0, 0, 0, 0.15)",
                                }}
                                id="input-example"
                                name="Price"
                                placeholder="Please enter a price"
                                defaultValue={15.0}
                                decimalsLimit={2}
                            />
                        </Row>

                        <Spacer y={2} />
                        <Text size={18}>Upload file</Text>
                        <FileUploader
                            handleChange={handleGameChange}
                            name="file"
                            types={fileTypesGame}
                        />
                        <Spacer y={2} />
                        <Text size={18}>Details</Text>
                        <Textarea
                            {...description.bindings}
                            width="100%"
                            label="Description"
                            helperText="This will make up the content of your game page"
                        />
                        <Spacer y={2} />
                        <Text size={18}>Game genre</Text>
                        <Dropdown>
                            <Dropdown.Button
                                flat
                                color="secondary"
                                css={{ tt: "capitalize" }}
                            >
                                {selectedGenreValue}
                            </Dropdown.Button>
                            <Dropdown.Menu
                                aria-label="Game genre"
                                color="secondary"
                                disallowEmptySelection
                                selectionMode="multiple"
                                selectedKeys={selectedGenres}
                                onSelectionChange={setSelectedGenres}
                            >
                                {renderGenre()}
                            </Dropdown.Menu>
                        </Dropdown>
                        <Spacer y={2} />
                        <Text size={18}>Operating Systems</Text>
                        <Dropdown>
                            <Dropdown.Button
                                flat
                                color="secondary"
                                css={{ tt: "capitalize" }}
                            >
                                {selectedOSValue}
                            </Dropdown.Button>
                            <Dropdown.Menu
                                aria-label="operating systems"
                                color="secondary"
                                disallowEmptySelection
                                selectionMode="multiple"
                                selectedKeys={selectedOSes}
                                onSelectionChange={setSelectedOS}
                            >
                                {renderOSes()}
                            </Dropdown.Menu>
                        </Dropdown>
                        <Spacer y={2} />
                        <Button disabled={!validateFields()} onPress={onSubmit} css={{ borderRadius: 0 }} auto>
                            Save & view page
                        </Button>
                        <Spacer y={2} />
                    </Col>
                    <Col span={4} css={{ m: 10 }}>
                        <Text size={18}>Cover image</Text>
                        <div>
                            {coverFile.map((file, idx) => (
                                <Image alt="cover" key={`file-cover_${idx}`} src={file.src}></Image>
                            ))}
                        </div>
                        <FileUploader
                            handleChange={handleChangeCover}
                            name="file"
                            types={fileTypes}
                        />
                        <Spacer y={2} />
                        <Text size={18}>Header image</Text>
                        <div>
                            {headerFile.map((file, idx) => (
                                <Image alt="header" key={`file-header_${idx}`} src={file.src}></Image>
                            ))}
                        </div>
                        <FileUploader
                            handleChange={handleChangeHeader}
                            name="file"
                            types={fileTypes}
                        />
                        <Spacer y={2} />
                        <Text size={18}>Screenshots</Text>
                        <div>
                            {screenshots.map((file, idx) => (
                                <Image alt="screenshot" key={`screenshot_${idx}`} src={file.src}></Image>
                            ))}
                        </div>
                        <FileUploader
                            handleChange={handleChangeScreenshots}
                            name="file"
                            types={fileTypes}
                            multiple
                        />
                    </Col>
                </Row>
            </Card>
        </Container>
    );
}

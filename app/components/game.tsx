"use client";
import { Container, Row, Col, Image, Text, Collapse, Badge, Button, Spacer, Modal } from "@nextui-org/react";
import { getFindGameByPageUrl } from "../api/game_api";
import React from "react";
import { buyGame, getGame, getUserHasGame } from "../api/user_api";
import { useSession } from "next-auth/react";
import { BASE_PATH } from "../api/api";
import { useRouter } from "next/navigation";
import { SWRRequest } from "../api/baseapi";

export default function Game({ pathParamName }: { pathParamName: string }) {
    const session = useSession();
    const router = useRouter();
    const token = session.data?.jwt;
    const [visible, setVisible] = React.useState(false);
    const [shouldBuy, setShouldBuy] = React.useState(false);
    const [shouldDownload, setShouldDownload] = React.useState(false);
    const handler = () => setVisible(true);

    const hasGameRequest = getUserHasGame(pathParamName, token);
    const { data, error, isLoading } = getFindGameByPageUrl(pathParamName);
    const buyGameRequest = buyGame(shouldBuy, data?.id, token);


    const fetcher = async (request: SWRRequest) => {
        setShouldDownload(false);
        await fetch(request.url, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${request.authToken}`,
                'Content-Type': 'application/octet-stream',
            },
        })
            .then((response) => response.blob())
            .then((blob) => {
                const url = window.URL.createObjectURL(
                    new Blob([blob]),
                );
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute(
                    'download',
                    data.builds[0].name,
                );
                document.body.appendChild(link);
                link.click();
                link?.parentNode?.removeChild(link);
            });
    };

    const downloadGameRequest = getGame(shouldDownload, data?.id, token, fetcher);
    const download = () => {
        setShouldDownload(true);

    };
    const goTo = (path: string) => {
        router.push(path);
    };
    const closeHandler = () => {
        setVisible(false);
        setShouldBuy(true);
        if (buyGameRequest.error) console.log(buyGameRequest.error);
        if (!isLoading)
            goTo(`/games_library`);
    };
    if (error) return "An error has occurred.";
    const renderImages = () => {
        if (isLoading) return <div>Loading...</div>;
        return data.images.map((img, j) => {
            return <div key={img.id} style={{ marginBottom: "10px" }}>
                <Image
                    width="100%"
                    height={180}
                    src={`${BASE_PATH}/Content/${data.id}/${img.id}`}
                    alt="Default Image"
                    objectFit="cover"
                />
            </div>
        })
    };

    const renderBuyOrDownload = () => {
        if (hasGameRequest.data) {
            return <Button auto css={{ borderRadius: 0 }} onPress={download}>Download</Button>
        } else {
            return (
                <div>
                    <Button auto css={{ borderRadius: 0 }} onPress={handler}>BUY</Button>
                    <Spacer x={1} />
                    <Text css={{ mt: 6 }}>${data.prices[0]?.value} USD</Text>
                </div>
            )
        }
    };

    const renderContent = () => {
        if (isLoading) {
            return <Container css={{ minHeight: "100vh", width: "50vw", backgroundColor: "#FFFFFF", p: 0 }} />
        } else {
            return (<Container css={{ minHeight: "100vh", width: "50vw", backgroundColor: "#FFFFFF", p: 0 }} >
                <Row>
                    <Col>
                        <Image
                            width="100%"
                            height={180}
                            src={`${BASE_PATH}/Content/${data.id}/${data.headerImage.id}`}
                            alt="Default Image"
                            objectFit="cover"
                        />
                        <Row css={{ p: 20 }}><Text b size={20}>{data.title}</Text></Row>
                        <Row css={{ p: 20 }}><Text>{data.shortDescription}</Text></Row>
                        <Row css={{ p: 20 }}>
                            {renderBuyOrDownload()}
                        </Row>
                    </Col>
                </Row >
                <Row css={{ p: 20 }}>
                    <Col span={7}>
                        <Row><Text>{data.description}</Text></Row>
                        <Collapse.Group>
                            <Collapse title="More info">
                                <Text>
                                    Updated At:{new Date(data.builds[0]?.updatedAt).toLocaleString()}
                                </Text>
                                <Text>
                                    Created At:{new Date(data.builds[0]?.createdAt).toLocaleString()}
                                </Text>
                                <Text>
                                    Status:{data.status}
                                </Text>
                                <Text>
                                    OS:{data.oses.map((os, j) => (
                                        <Badge key={os.id}>{os.name}</Badge>
                                    ))}
                                </Text>
                                <Text>
                                    Author:{data.author}
                                </Text>
                                <Text>
                                    Genre:{data.genres.map((gen, j) => (
                                        <Badge key={gen.id}>{gen.name}</Badge>
                                    ))}
                                </Text>
                            </Collapse>
                        </Collapse.Group>
                        <Row><Text b>Comprar</Text></Row>
                        <Row>
                            {renderBuyOrDownload()}


                        </Row>
                    </Col>
                    <Col span={5}>
                        {renderImages()}
                    </Col>
                </Row >
            </Container >);
        }
    }
    return (
        <div>
            {renderContent()}
            <Modal
                closeButton
                aria-labelledby="modal-title"
                open={visible}
                onClose={closeHandler}
            >
                <Modal.Header>
                    <Text id="modal-title" size={18}>
                        Buy
                    </Text>
                    <Spacer x={1} />
                    <Text b size={18}>
                        {data?.title}
                    </Text>
                </Modal.Header>
                <Modal.Body>
                    <Row css={{ p: 20 }}>
                        <Text css={{ mt: 6 }}>${data?.prices[0]?.value} USD</Text>
                    </Row>
                </Modal.Body>
                <Modal.Footer>

                    <Button auto onPress={closeHandler}>
                        PayPal
                    </Button>
                    <Button auto onPress={closeHandler}>
                        Card
                    </Button>
                </Modal.Footer>
            </Modal>
        </div >
    );
}
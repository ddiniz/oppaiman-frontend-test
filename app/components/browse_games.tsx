"use client";
import {
    Badge,
    Button,
    Card,
    Col,
    Collapse,
    Container,
    Grid,
    Row,
    Spacer,
    Text,
} from "@nextui-org/react";
import { useRouter } from "next/navigation";
import { Buy, ChevronRight, Star, TimeCircle } from "react-iconly";

import { listGenres } from "../api/genre_api";
import { FilterGameRequest, GameResponse } from "../api/model/game";
import { GenreResponse } from "../api/model/genre";
import { objectToQueryParams } from "../api/utils";
import { getListGamesPaginated } from "../api/game_api";
import { BASE_PATH } from "../api/api";
import { useRef, useState } from "react";

function GetDayDate(days: number) {
    let fullDate = new Date();
    let now = new Date(
        fullDate.getFullYear(),
        fullDate.getMonth(),
        fullDate.getDate() - days
    );
    return now.getTime();
}

export default function BrowseGames({
    searchParams,
}: {
    searchParams?: { [key: string]: string | undefined };
}) {
    const router = useRouter();

    const filterGameRequest: FilterGameRequest = {
        name: searchParams?.name,
        price: searchParams?.price,
        onSale: searchParams?.onSale,
        releaseDate: searchParams?.releaseDate,
        genre: searchParams?.genre,
        limit: searchParams?.limit,
        page: searchParams?.page,
    };

    const [aux, aux2] = useState(filterGameRequest.price === "0")

    const changeFilterPrice = (val: number) => {
        if (filterGameRequest.price !== "" + val) {
            filterGameRequest.price = "" + val;
        } else {
            filterGameRequest.price = "-1";
        }
        router.push("/browse_games" + objectToQueryParams(filterGameRequest));
    };

    const changeFilterPriceOnSale = () => {
        if (filterGameRequest.onSale === "true") filterGameRequest.onSale = "false";
        else filterGameRequest.onSale = "true";
        router.push("/browse_games" + objectToQueryParams(filterGameRequest));
    };

    const changeFilterDate = (val: number) => {
        let millis = GetDayDate(val)
        if (filterGameRequest.releaseDate === "" + millis)
            filterGameRequest.releaseDate = "0";
        else filterGameRequest.releaseDate = "" + millis;
        router.push("/browse_games" + objectToQueryParams(filterGameRequest));
    };
    const changeFilterGenre = (val: string) => {
        if (filterGameRequest.genre !== val) filterGameRequest.genre = val;
        else filterGameRequest.genre = "";
        router.push("/browse_games" + objectToQueryParams(filterGameRequest));
    };
    const genreData = listGenres();
    const renderGenre = () => {
        if (genreData.isLoading) return <div>Loading...</div>;
        return genreData.data.map((item: GenreResponse) => (
            <Text key={item.id}>
                <Button
                    {...{ light: filterGameRequest.genre !== item.name }}
                    icon={<ChevronRight set="bold" />}
                    auto
                    onClick={() => {
                        changeFilterGenre(item.name);
                    }}
                >
                    {item.name}
                </Button>
            </Text>
        ));
    };

    const gamesData = getListGamesPaginated(filterGameRequest);
    let queryParams = objectToQueryParams(filterGameRequest);


    const renderGames = () => {
        if (gamesData.isLoading) return <div>Loading...</div>;
        return (
            <Grid.Container gap={2} justify="flex-start">
                {gamesData.data.rows.map((item: GameResponse) => (
                    <Grid css={{ width: "300px" }} key={item.id}>
                        <Card
                            isPressable
                            onPress={() => {
                                router.push(`/game/${item.pageUrl}`);
                            }}
                        >
                            <Card.Body css={{ p: 0 }}>
                                <Card.Image
                                    src={`${BASE_PATH}/Content/${item.id}/${item.catalogImage.id}`}
                                    objectFit="cover"
                                    width="100%"
                                    height={250}
                                    alt={item.title}
                                />
                            </Card.Body>
                            <Card.Footer css={{ justifyItems: "flex-start" }}>
                                <Col>
                                    <Row justify="space-between" align="center">
                                        <Text b>{item.title}</Text>
                                        <Text
                                            css={{
                                                color: "$accents7",
                                                fontWeight: "$semibold",
                                                fontSize: "$sm",
                                            }}
                                        >
                                            ${item?.prices[0]?.value}
                                        </Text>
                                    </Row>
                                    <Row justify="space-between" align="center">
                                        <Text>{item.shortDescription}</Text>
                                    </Row>
                                    <Row justify="space-between" align="center">
                                        <Text>{item.author}</Text>
                                    </Row>
                                    <Row align="center">
                                        {item.genres.map((gen, j) => (
                                            <Badge key={gen.id}>{gen.name}</Badge>
                                        ))}
                                    </Row>
                                    <Row align="center">
                                        {item.oses.map((os, j) => (
                                            <Badge key={os.id}>{os.name}</Badge>
                                        ))}
                                    </Row>
                                </Col>
                            </Card.Footer>
                        </Card>
                    </Grid>
                ))}
            </Grid.Container>
        );
    };
    if (gamesData.error || genreData.error) return "An error has occurred.";
    return (
        <Container css={{ minHeight: "100vh" }}>
            <Row>
                <Col span={2}>
                    <Spacer y={2} />
                    <Text b>FILTER RESULTS</Text>
                    <Collapse.Group accordion={false}>
                        <Collapse title="Price">
                            <Text>
                                <Button
                                    {...{ light: filterGameRequest.price !== "0" }}
                                    icon={<Star set="bold" />}
                                    auto
                                    onClick={() => {
                                        changeFilterPrice(0);
                                    }}
                                >
                                    Free
                                </Button>
                            </Text>
                            <Text>
                                <Button
                                    {...{ light: filterGameRequest.onSale !== "true" }}
                                    icon={<Star set="bold" />}
                                    auto
                                    onClick={() => {
                                        changeFilterPriceOnSale();
                                    }}
                                >
                                    On Sale
                                </Button>
                            </Text>
                            <Text>
                                <Button
                                    {...{ light: filterGameRequest.price !== "5" }}
                                    icon={<Buy set="bold" />}
                                    auto
                                    onClick={() => {
                                        changeFilterPrice(5);
                                    }}
                                >
                                    $5 or less
                                </Button>
                            </Text>
                            <Text>
                                <Button
                                    {...{ light: filterGameRequest.price !== "15" }}
                                    icon={<Buy set="bold" />}
                                    auto
                                    onClick={() => {
                                        changeFilterPrice(15);
                                    }}
                                >
                                    $15 or less
                                </Button>
                            </Text>
                        </Collapse>
                        <Collapse title="When">
                            <Text>
                                <Button
                                    {...{ light: filterGameRequest.releaseDate !== "" + GetDayDate(1) }}
                                    icon={<TimeCircle set="bold" />}
                                    auto
                                    onClick={() => {
                                        changeFilterDate(1);
                                    }}
                                >
                                    Last Day
                                </Button>
                            </Text>
                            <Text>
                                <Button
                                    {...{ light: filterGameRequest.releaseDate !== "" + GetDayDate(7) }}
                                    icon={<TimeCircle set="bold" />}
                                    auto
                                    onClick={() => {
                                        changeFilterDate(7);
                                    }}
                                >
                                    Last 7 days
                                </Button>
                            </Text>
                            <Text>
                                <Button
                                    {...{ light: filterGameRequest.releaseDate !== "" + GetDayDate(30) }}
                                    icon={<TimeCircle set="bold" />}
                                    auto
                                    onClick={() => {
                                        changeFilterDate(30);
                                    }}
                                >
                                    Last 30 day
                                </Button>
                            </Text>
                        </Collapse>
                        <Collapse title="Genre">{renderGenre()}</Collapse>
                    </Collapse.Group>
                </Col>
                <Col span={10}>{renderGames()}</Col>
            </Row>
        </Container>
    );
}

"use client";
import {
    Button,
    Card,
    Checkbox,
    Container,
    Input,
    Link,
    Row,
    Spacer,
    Switch,
    Text,
    useInput,
} from "@nextui-org/react";
import { useRef, useState, useMemo } from "react";
import { postCreateUser } from "../api/user_api";
import { useRouter } from "next/navigation";

export default function Signup() {
    const username = useInput("");
    const password = useInput("");
    const passwordRepeat = useInput("");
    const email = useInput("");
    const developerRole = useRef(false);
    const [terms, setTerms] = useState(false);
    const router = useRouter()
    const goTo = (path: string) => {
        router.push(path)
    };
    const onSubmit = () => {
        if (validateFields()) {
            postCreateUser({
                username: username.value,
                email: email.value,
                password: password.value,
                role: Number(developerRole.current),
            }).then((data) => goTo("/"));
        }
    };
    const validateFields = () => {
        return username.value.length > 0
            && password.value.length > 0
            && email.value.length > 0
            && terms
            && password.value === passwordRepeat.value;
    }
    return (
        <Container
            display="flex"
            alignItems="center"
            justify="center"
            css={{ minHeight: "100vh" }}
        >
            <Card css={{ mw: "50vw", p: "20px", borderRadius: 0 }} variant="bordered">
                <Card.Header>
                    <Text
                        size={24}
                        weight="bold"
                        css={{
                            as: "center",
                            mb: "20px",
                        }}
                    >
                        Create an account
                    </Text>
                </Card.Header>
                <Input
                    {...username.bindings}
                    labelPlaceholder="Username"
                    clearable
                    underlined
                    fullWidth
                    color="primary"
                    size="lg"
                    placeholder="Username"
                />
                <Spacer y={2} />
                <Input.Password
                    {...password.bindings}
                    labelPlaceholder="Password"
                    clearable
                    underlined
                    fullWidth
                    color="primary"
                    size="lg"
                    placeholder="Password"
                    type="password"
                />
                <Spacer y={2} />
                <Input.Password
                    {...passwordRepeat.bindings}
                    labelPlaceholder="Repeat Password"
                    clearable
                    underlined
                    fullWidth
                    color="primary"
                    size="lg"
                    placeholder="Password"
                    type="password"
                />
                <Spacer y={2} />
                <Input
                    {...email.bindings}
                    labelPlaceholder="Your e-mail address"
                    clearable
                    underlined
                    fullWidth
                    color="primary"
                    size="lg"
                    placeholder="name@example.com"
                    type="email"
                />
                <Spacer y={1} />
                <Row>
                    <Switch
                        squared
                        color="primary"
                        checked={developerRole.current}
                        onChange={(e) => (developerRole.current = e.target.checked)}
                    />
                    <Spacer x={.5} />
                    <Text b css={{ mt: 3 }}>
                        {" "}
                        Im a games Developer
                    </Text>
                </Row>
                <Spacer y={1} />
                <Row justify="space-between">
                    <Checkbox isSelected={terms}
                        onChange={setTerms}>
                        <Text size={14}>
                            I accept the <Link href="/terms">terms of service</Link>
                        </Text>
                    </Checkbox>
                </Row>
                <Spacer y={1} />
                <Row>
                    <Button disabled={!validateFields()} onClick={onSubmit} css={{ borderRadius: 0 }} auto>
                        Create Account
                    </Button>
                    <Spacer x={1} />
                    <Text size={14}>
                        {" "}
                        or already have an account? <Link href="/">Login</Link>
                    </Text>
                </Row>
                <Row>
                    <Text size={14} hidden={password.value === passwordRepeat.value}
                        color="red">
                        Passwords do not match!
                    </Text>
                </Row>
            </Card>
        </Container>
    );
}

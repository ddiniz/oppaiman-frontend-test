import { Inter } from "next/font/google";
import "./globals.css";
import { NextAuthProvider } from "./providers";
import Navbar from "./components/navbar";
import StyledComponentsRegistry from './registry'
const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Oppaiman",
  description: "Oppaiman",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>

        <NextAuthProvider>
          <StyledComponentsRegistry>
            <Navbar></Navbar>
            {children}
          </StyledComponentsRegistry>
        </NextAuthProvider>
      </body>
    </html>
  );
}




